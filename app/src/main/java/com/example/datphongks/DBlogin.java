package com.example.datphongks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBlogin extends SQLiteOpenHelper {

    public DBlogin(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE \"ACCOUNT\" (\n" +
                "\t\"ID\"\tINTEGER,\n" +
                "\t\"USERNAME\"\tINTEGER,\n" +
                "\t\"PASSWORD\"\tINTEGER,\n" +
                "\tPRIMARY KEY(\"ID\")\n" +
                ")";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
